# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###

This repository is used to generate the academy videos for ese > develop.

## Vorbedingungen
Das Narakeet Client-Tool wurde installiert: https://www.narakeet.com/docs/automating/cli/

## Videos neu generieren
Dazu einfach ```./generate.sh``` ausführen. Die generierten Videos werden in den Ordner "out" gespeichert.

## Ordner
* folie = Originale pptx-Folien zum exportieren als png
* out = Gerenderte Videos landen nach der Ausführung von generate.sh hier
* source = Alle notwendigen Dateien um die Videos zu generieren

## Änderungen im Text
Änderungen im Text können einfach im entsprechenden Kapitel in die Markdown-Datei geschrieben werden. Diese findet man direkt unter ```source/chapterXX.md```.

## Änderungen der Folien
Änderungen der Folien sollten in der originalen Powerpoint-Präsentation gemacht werden. Sind diese fertiggestellt, können die einzelnen Folien direkt über einen File>Export in das png-Format im jeweiligen Order eines Kapitels ```source/slides/chapterXX``` gespeichert werden.

## Stimmen ändern
Verfügbare Stimmen findet man hier: https://www.narakeet.com/docs/voices/#german

## Sonstige Markdown-Formatierungen
Die Formatierungen für das Markdown sind hier zu finden: https://www.narakeet.com/docs/format/#adjusting-background-music-volume

## Inhalte ese > develop

Confluence-Inhalte:
https://esentri.atlassian.net/wiki/spaces/KKVORSPRUNG/pages/3503056249/2.+Inhalt+ese+develop+Framework

Google Drive Inhalte:
https://drive.google.com/drive/folders/10dkRWLmVPK4Qe66Q3g8mtk47opMotefi?usp=sharing

Miro Boards:
https://miro.com/app/dashboard/?spaceId=3074457362993950542&projectId=3074457366070242009


