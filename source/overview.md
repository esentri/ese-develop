---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/overview/Folie1.png)

(pause: 2)

Hallo und herzlich willkommen zu einem Überblick über das easy deewellopp Framework. Dieses Video soll euch einen Überblick über die Phasen geben, die in folgenden Videos detailliert betrachtet werden.

(pause: 1)

easy deewellopp ist unser Rahmenwerk um ganzheitlich Projekte zu realisieren. Von der Akquise über die Entwicklung. bis zum Abschluss des Projektes mit der Übergabe an den Kunden.

(pause: 1)

Das easy deewellopp ist aus issentri heraus - in erster Linie durch den Geschäftskreis Digitale Kultur und in Gesprächen mit allen Geschäfts- und Dienstleistungskreisen - entstanden. Wie Software auch, befinden wir uns in einem stetigen Weiterentwicklungsprozess. Dennoch gibt es einige Standards die sich bewährt und etabliert haben und sich lohnen in einem Framework festzuhalten.

(pause: 2)
---
![contain](slides/overview/Folie2.png)

Das easy deewellopp Framework unterteilt sich in fünf Bereiche.

(pause: 1)

Erstens: Akquise. Wie kommen wir eigentlich zu einem Projekt. Welche Prozesse müssen wir durchlaufen, um ein hoch qualitatives Angebot abgeben zu können. Diese Phase ist den anderen vorangestellt und Bedingung, um das easy deewellopp Framework einsetzen zu können.
Zweitens: Prepare. Hier wird der Projektstart vorbereitet.

(pause: 1)

Drittens und Viertens: Initiate & deewellopp. Die tatsächliche Produktentwicklung für den Kunden.
Fünftens und letztens: [Transition]{en}: Die Übergabe des erfolgreich entwickelten Produktes.

(pause: 2)
---
![contain](slides/overview/Folie3.png)

Im Akquise Teil werdet ihr kennenlernen, warum diese Phase wichtig für alle Mitarbeitenden bei issentri ist. Außerdem findet ihr im Akquise Bereich der Academy auch das Sales Deck, mit welchem wir bei potenziellen Kunden auftreten. Wir vermitteln, was es bedeutet mit uns ein Projekt zu machen und was auf High Level die gemeinsame Basis sein muss. Des Weiteren sind Inhalte der Akquise Phase der Request for Proposal Prozess sowie das Erstellen einer Schätzung. Agil hin, Agil her: der Kunde verlangt zurecht einen Plan und ein Preisschild.

(pause: 2)
---
![contain](slides/overview/Folie4.png)

Nach erfolgreicher Akquise erfolgt die Prepare Faahse. Oftmals wird zu Beginn nicht ausreichend Qualität in der Vorbereitung sichergestellt, was sich im späteren Verlauf eines Projekts als Grund des Scheiterns herausstellen kann. Deshalb legen wir einen großen Wert auf die Planung mehrere Wochen vor dem ersten Sprint. Denn weder soll das Requirement Engineering der Entwicklung nicht hinterherkommen, noch soll die Entwicklung die Anforderungen der Fachlichkeit sowie der Softwarearchitektur übergehen. Außerdem werden Tools, Infrastruktur und technische Zugänge vorbereitet um von Tag eins an arbeitsfähig zu sein.

(pause: 1)

Die relevanten Bereiche hier sind Business & Project Management, Software Architecture sowie DevOps & Product Support.

(pause: 2)
---
![contain](slides/overview/Folie5.png)

Auf die Prepare Faahse folgt die Initiate & Develop Faahse. Hier der Überblick über easy deewellopp, wenn man sich in der tatsächlichen Produktentwicklung befindet.

(pause: 2)

Schematisch dargestellt sind mögliche Akteure, die langfristige Release Planung, die Produktentwicklung, sowie die Essentials. Sie zeigen welche Themen von essenzieller Bedeutung in dieser Phase sind.

(pause: 2)
---
![contain](slides/overview/Folie6.png)

Last but not least: [Transition]{en}. Ziel dieser Phase ist, dass das Produkt wie vorher vereinbart dem Kunden übergeben und er gleichzeitig befähigt wird das Ergebnis auch entsprechend einsetzen oder sogar weiterentwickeln zu können.

(pause: 2)
---
![contain](slides/overview/Folie1.png)

Das war ein kurzer Überblick über das gesamte easy deewellopp Framework. In den folgenden Kursen werden wir jede einzelne Phase im Detail betrachten.

(pause: 1)

Bis dahin: vielen Dank für Deine Zeit!
---
