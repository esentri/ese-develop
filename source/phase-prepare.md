---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/phase-prepare/Folie1.png)

(pause: 2)

Hallo und herzlich willkommen zum zweiten Video der easy deewellopp Reihe.

In diesem Video dreht sich alles um die Prepare Faahse. Ich bin Ilse und habe die Ehre euch heute die neuen Lerninhalte zu übermitteln.
Wie ihr sicher wisst ist eine gute Vorbereitung der Grundstein für ein erfolgreiches Projekt. Genau das unterstützt die Prepare Faahse.

Starten wir direkt durch und ordnen die Prepare Faahse erst einmal in den Gesamtkontext von easy deewellopp ein.

---
(pause: 2)

![contain](slides/phase-prepare/Folie2.png)

Nach der Akquise Phase, die ihr bereits kennengelernt habt, folgt die Prepare Faahse. Gehen wir davon aus, dass das Angebot aus der Akquise Phase angenommen wurde und wir uns mit dem Kunden einig geworden sind. Bevor wir planlos in die Tasten hauen, möchten wir uns vorbereiten. Die Prepare Faahse erlaubt uns nicht in typische Projektfallen zu tappen und schon zu Beginn eines Projekts grundlegende Fehlentscheidungen zu treffen. Wir legen in dieser Phase den Grundstein für ein erfolgreiches Projekt.

(pause: 2)
---
![contain](slides/phase-prepare/Folie3.png)

Die Vorbereitungen können in drei Teilbereiche eingeordnet werden. Bissness and Project Management, sowie Software Architecture, und schließlich: Devops and Product Support. In allen Teilbereichen verfolgt easy deewellopp spezifische Ziele.

(pause: 2)
---
![contain](slides/phase-prepare/Folie4.png)


Innerhalb des Bereichs "Bissness and Project Management" werden folgende Ziele verfolgt:

erstens: Etablierung eines gemeinsamen Zielbilds.
zweitens: Entwicklung einer Product Roadmap.
drittens: Grobe Release Planung.
viertens: Detailplanung des ersten Releases.
und fünftens: Training von Product Owner und Bissness Analysten des Kunden.

(pause: 2)
---
![contain](slides/phase-prepare/Folie5.png)

Im Bereich Software Architecture möchten wir erstens: Qualitätsszenarien erarbeiten, zweitens: eine Architekturdokumentation erstellen, drittens: Rahmenbedingungen der Softwarearchitektur definieren und schließlich erste grundlegende Designentscheidungen treffen.

(pause: 1)
---

![contain](slides/phase-prepare/Folie6.png)

Im Bereich Devops And Product Support verfolgen wir folgende Ziele:
erstens: Beantragung und Einrichtung von Zugängen für alle Projektmitglieder.
zweitens: Aufbau von Umgebungen auf denen unsere Software betrieben wird.
drittens: Aufbau von continuous Integration und continuous Development Infrastruktur.
und viertens: Die Konfiguration von Projekt Management Tools.

(pause: 2)
---
![contain](slides/phase-prepare/Folie7.png)

Diese Ziele sollen in einem Zeitrahmen von fünf Wochen erreicht werden. Die verschiedenen Teilbereiche sind in zeitliche Abschnitte eingeteilt.

(pause: 1)

Wir sehen, dass Bissness and Project Management von Woche eins bis Woche fünf durchgehend mit Themen bespielt wird. Zu Beginn gilt es ein gemeinsames Verständnis für das Projekt und die Projektziele zu definieren.

(pause: 1)

In Woche 2 legen die Projektmitglieder eine Roadmap und Epics fest. Danach bleiben noch zweieinhalb Wochen Zeit erste detaillierte Requirements zu erfassen und einen Backlog zu erstellen und auszuarbeiten.

(pause: 2)

Schließlich startet die Operative Phase des Projekts mit einem Kick Off Event am Ende der Woche fünf. Parallel dazu starten Devops und Product Support, sowie Software Architecture in Woche drei der Prepare Faahse. Neben der Definition der Softwarearchitektur werden die Umgebungen und Werkzeuge bereitgestellt.

(pause: 1)

Dieser Ablauf ist natürlich schematisch dargestellt und trifft nicht auf jedes Projekt zu. Die drei Bereiche können individuell organisiert werden. Wichtig ist den genannten Zielen in den Teilbereichen nahe zu kommen, um  gut vorbereitet zu sein, wenn die Arbeit am ersten Release beginnt.

(pause: 2)
---
![contain](slides/phase-prepare/Folie8.png)


Zum Schluss noch einmal zusammengefasst: Die Prepare Faahse legt den Grundstein für ein erfolgreiches Projekt. Vorbereitungen in den Bereichen: Bissness and Project Management sowie Software Architecture, und schließlich: Devops and Product Support. Nach fünf Wochen Vorbereitung folgt ein Kickoff Event und der Übergang zur nächsten Phase: Der Initiate and Develop Faahse.

(pause: 1)

Vielen Dank fürs Zuhören, ich hoffe ihr konntet mir folgen und habt Lust auf mehr!
Ilse bedankt sich bei den Lernenden.

(pause: 1)

Bis zum nächsten Video!

(pause: 2)

---
