---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.02 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/phase-initiate-develop/Folie1.png)

(pause: 2)

Hallo Ich bins wieder eure Ilse, und damit willkommen zurück zur easy deewellopp Lernreihe.
Wie schon in den vorherigen Videos führe ich euch heute durch die Inhalte.

(pause: 1)

Heute lernen wir die Initiate und Develop Faahse kennen, man könnte sagen - dass Herzstück des Frameworks.

(pause: 1)

Legen wir also direkt los, wir haben einiges Vor uns.

Zunächst ordnen wir die Initiate und Develop Phase in den Gesamtkontext von easy deewellopp ein.

---
(pause: 2)

![contain](slides/phase-initiate-develop/Folie2.png)

Initiate und Develop das sind genau genommen Phase Zwei und Phase Drei des Frameworks. Inhaltlich gehören sie eng zueinander, nach einer gelungenen Prepare Faahse geht es mit der Projektumsetzung erst so richtig los. Haben sich die Akteure eingespielt, folgt Release an Release. Anforderungen werden erhoben, Softwareinkremente entwickelt, alles in agiler und iterativer Arbeitsweise, so sieht der Kunde seine Anwendung stetig nach seinen Vorstellungen wachsen. Dabei hinterfragen die Akteure Ihre Prozesse und Arbeitsweisen um sich kontinuierlich weiterzuentwickeln. Sie folgen dem Prinzip: Inspect and Adapt.
---
(pause: 2)

![contain](slides/phase-initiate-develop/Folie3.png)

So viel zur Übersicht, jetzt lernen wir die Kernbereiche der Phase kennen:
Es gibt vier Bereiche die ich euch heute genauer vorstellen möchte.

(pause: 1)

Bereich eins: Die Akteure und Ihre Rollen im Framework.
Bereich zwei: die Agile Langzeitplanung.
Bereich drei: Die iterative Produktentwicklung, und schließlich
Bereich vier: Agile Essentials.

(pause: 2)
---
(pause: 2)

![contain](slides/phase-initiate-develop/Folie4.png)

Die am Framework beteiligten Akteure setzen sich aus den folgenden Rollen zusammen:

(pause: 1)

Der Product Owner:

Ein Product Owner  verantwortet die ganzheitliche Produktentwicklung. Zu den wichtigsten Aufgaben gehören:

die Schaffung eines gemeinsamen Verständnisses der Produktvision

die Definition einer Roadmap inklusive Release-Planung

die Erarbeitung eines nach Nutzen und Wert priorisierten Backlogs für die Produktentwicklung.
Der P O kann das Produkt und seine Entwicklung also sehr gut beschreiben und weiß, was es kann und was nicht. Idealerweise auf einer sehr tiefen, fachlichen Ebene.

Der Product Owner ist (auf Projektebene) unser wichtigster und erster Ansprechpartner beim Kunden. Unser Bestreben ist, ihn bestmöglich zur Erreichung der Projekt- und Produktziele zu beraten, aber auch Entscheidungen zu erfolgskritischen Fragestellungen von ihm einzufordern. Es geht ausdrücklich nicht darum, dem Product Owner “nach dem Mund” zu reden, nur um ihn zufrieden zu stellen, sondern ehrlich und transparent die Lage inklusive womöglich unangenehmer Wahrheiten und Erkenntnisse darzustellen - idealerweise inklusive Lösungsoptionen. Nur so erhält der P O die relevanten und notwendigen Informationen zur erfolgreichen Steuerung und Planung.


(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie5.png)

Es folgt der sogenannte Proxy P O.

Dieser unterstützt den Product Owner des Kunden bei seinen Aufgaben, wenn dieser aus unterschiedlichen Gründen seine Rolle nicht angemessen ausfüllen kann. Der Proxy P O wird dann in der Regel durch einen internen Mitarbeiter besetzt und arbeitet operativ eng mit dem P O zusammen.

(pause: 1)

Unser Auftrag als Proxy P O ist es einerseits, den P O so zu begleiten und zu coachen, dass dieser auf Sicht die notwendigen Kompetenzen entwickelt und eigenständig agieren kann. Andererseits erfüllt der Proxy P O operative P O Aufgaben. Dazu arbeitet er eng mit dem Business Analysten Team zusammen und braucht ein gutes Verständnis der Fachlichkeit.
Wichtig ist uns hierbei aber auch, dass projektkritische Entscheidungen final immer durch den Kunden, also den P O, getroffen werden. Er hat das letzte Wort. Die Entscheidungsvorlage und Empfehlungen dazu liefert der Proxy P O und oder das Business Analysten Team und oder das Development Team.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie6.png)

Die Business Analysten analysieren und identifizieren die Fachlichkeit und Anforderungen des Kunden hinsichtlich des gewünschten Produkts. Sie bewerten und visualisieren die Anforderungen hinsichtlich Nutzen sowie Ausbaustufen und Optionen.
als Entscheidungsvorlage und Empfehlung für den P O führen sie ein Sparring mit den Software Architekten durch.
zur technischen Validierung der fachlichen Anforderungen spezifizieren sie die vom P O gewünschten Anforderungen und füllen somit das Product Backlog zur Implementierung durch das Development Team.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie7.png)

Das Development Team hat die Verantwortung die definierten und priorisierten Backlog Items entsprechend der Kundenintention umzusetzen. Das Team ist den Anforderungen entsprechend Cross-Funktional aufgestellt. Somit kann es den gesamten Entwicklungsprozess der Kundenanforderungen bis zur Produktionsreife begleiten. Die Zusammenstellung des Development Teams wird je nach Kundenanforderungen variiert. Generell bewegt sich die Teamgröße im Rahmen von 3 bis 7 Spezialisten.

Das Development Team plant eigenständig wie die definierten Backlog Items umzusetzen sind. Das Development Team ist außerdem für die Sicherstellung der Qualität der umzusetzenden Backlog Items verantwortlich.

Wir glauben, dass das Development Team fokussiert an den Aufgaben arbeiten muss, um die vereinbarten Anforderungen und Ziele zu erfüllen. Dazu sensibilisieren wir das Team so, dass jede und jeder jederzeit einschätzen kann, was gerade wichtig ist und Mehrwert bringt. Wir haben den Ansatz, alle Arbeitspakete jederzeit transparent zu machen, sodass wir die Zusammenarbeit erhöhen und potenzielle Konflikte frühzeitig entdecken können.

(pause: 1)

Wir entscheiden nicht dogmatisch, ob alle Entwickler*innen Full-Stack oder Spezialisten für [Backend]{en} , Frontend, oder ähnliches sein müssen. Solche Entscheidungen lassen sich nicht pauschalisieren. Wir achten sehr stark darauf, keine Kopfmonopole zu bilden und entscheiden abhängig von der Teamgröße, unserem Vorhaben, organisatorischen Rahmenbedingungen und dem Skill-Set des Teams.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie8.png)

Der Scrum Master:
Ein Scrum Master schafft die organisatorischen Rahmenbedingungen für eine erfolgreiche agile Produktentwicklung. Die Kernaufgaben des Scrum Masters sind das Coaching des gesamten Produktteams, die Etablierung geeigneter Strukturen und Tools für ein agiles Projektmanagement, und die Lösung von Impediments des Produktteams innerhalb sowie außerhalb des Teams.

Der Scrum Master ist zuständig für die Erhöhung der Effektivität im Team. Dies kann unter anderem mit einem Coaching in Organisation und fokussierter Arbeit geschehen. Er oder Sie kann Hindernisse und fehlerhafte Prozesse methodisch entdecken, auch wenn diese nicht offensichtlich sind.

Er oder Sie hilft dem Development Team, Konflikte und organisatorische Probleme zu lösen, damit sich das Development Team auf die Produktentwicklung konzentrieren kann.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie9.png)

Last but not least der Software Architect:

Ein Software Architekt erarbeitet die relevanten Qualitätskriterien des Produktes und definiert mit dem Produktteam zusammen architekturelle Rahmenbedingungen und Grundsatzentscheidungen. Er oder Sie verantwortet damit den langfristigen technischen Erfolg des entwickelten Produktes.

Um die Architekturentscheidungen zu treffen, bewegt sich ein Software Architect an der Schnittstelle des Development Teams und Business Analysten, um die fachlichen Anforderungen und die Domäne richtig in die technische Architektur übersetzen zu können.

Er oder Sie ist in der Lage, im Development Team mitentwickeln zu können. Er oder Sie kennt das Know-How im Development Team und berücksichtigt das sowie das Feedback des Development Teams bei den Architekturentscheidungen. Er oder Sie kann erkennen, ob die Entwickler*innen eine Weiterbildung oder weiteres Coaching brauchen.

Ein Software Architect ist Ansprechpartner bei Architektur und technischen Fragen im Development Team. Er oder Sie kann der bei Entscheidungsfindung helfen, wenn sich das Development Team nicht einig ist, welche Lösung besser geeignet ist. Dabei kann Er oder Sie die Vor- und Nachteile der Entscheidungen objektiv auflisten und gegebenenfalls auch mit helfen, Konflikte zu lösen.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie10.png)

Nach dem wir nun die Akteure des Frameworks kennen, möchte ich Euch die Grundwerte des Frameworks näher bringen, wir nennen sie die easy Essentials.

(pause: 1)

Punkt eins: Kundenzentrierung:

Kundenzentrierung ist ein wichtiger Faktor für eine erfolgreiche Produktentwicklung. Als Konsequenz binden wir die Kunden in allen Entwicklungsphasen mit geeigneten Methoden ein. Im Mittelpunkt stehen für uns dabei die Sprint und Release-Reviews. Hierbei können alle Beteiligten einen Eindruck des aktuellen Inkrements Produktes gewinnen. Eine erfolgreiche Produktplanung kann dabei aber nicht nur als Summe erfolgreicher Inkremente betrachtet werden. Um Überhaupt zu verstehen wann ein Inkrement bzw. Produkt erfolgreich sein kann, ist es erforderlich, wichtige Einsichten zu generieren welche Eigenschaften das Ergebnis besitzen muss, um das Ziel zu erfüllen. Unsere Kunden versuchen wir dabei zunächst durch Design Thinking Workshops, Interviews und Job Watching dabei zu unterstützen, das Ziel zu beantworten ohne das (Wie) zu definieren. Durch Customer Journey Maps und die Definition von geeigneten Zielbildern bringen wir anschließend gemeinsam Klarheit in das Wie!

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie11.png)

Punkt zwei: Zielbilder:

Zielbilder helfen Menschen und Unternehmen die eigene Arbeit im größeren Kontext zu sehen. Nur wenn klar ist, wohin die Reise geht, entstehen die nötige Motivation und Dynamik, um das gesetzte Ziel zu erreichen. Ein Zielbild muss zu beginn erstellt und regelmäßig validiert werden. Dafür nutzen wir wiederum bekannte [Ideation]{en} und Kreativ-Methoden wie z.B. Vision Workshops, Start with Why, How Might We oder Double Diamond.  

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie12.png)

Punkt drei: Qualität:

Qualität ist in der Produktentwicklung die Basis für robuste Produkte, welche den Kundenbedarf abdecken, und eine konstante Entwicklungsgeschwindigkeit gewährleisten. Zu Beginn eines Projektes gilt es die gewünschten Qualitäten eines Produktes zu definieren und die Entwicklung daran auszurichten. Wir berücksichtigen bei der Konzeption des Systems nicht nur die funktionalen sondern auch die nicht funktionalen Anforderungen an die Software. Gerade die nicht funktionalen Anforderungen formulieren die Qualitätsansprüche der Software. Um die Systemkonzeption und Architektur strukturiert und einheitlich abzulegen, setzen wir auf den anerkannten Standard arc 42.

Da die Codebasis die Grundlage für das Produkt darstellt und maßgeblich die Entwicklungsgeschwindigkeit beeinflusst, haben wir hohe Qualitätsansprüche an diese. Durch Clean Code und die [solid]{en} Prinzipien wollen wir Code entwickeln, der dazu beiträgt, die Kundenbedürfnisse zu erfüllen. Wir glauben daran, dass Code so häufig wie möglich integriert werden muss und setzen deshalb auf Continuous Integration. Wir sind überzeugt dass Teams, die Code häufig in einem gemeinsamen Branch integrieren, eine bessere Performance und Codequalität sowie weniger Integrationsprobleme haben.

Wir glauben, dass jede Zeile der Codebasis getestet werden muss. Um dies sicherzustellen, setzen wir auf Test Driven Development. Damit wird gewährleistet, dass die Qualität der Testfälle erhöht wird und das Verständnis bei der Entwicklung für die zu erfüllende Funktionalität von Beginn an gegeben ist. Für uns steht der Nutzer der Software im Zentrum. Darum setzen wir auf Usability Tests um die in der Entwicklung getroffenen Hypothesen über die Funktionalität zu verifizieren.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie13.png)

Punkt vier Automatisierung:

Automatisierung hilft die Komplexität und Dynamik moderner Software und agiler Vorgehensmodelle beherrschbar zu machen. Nur durch einen hohen Automatisierungsgrad, kann eine gleichbleibende Entwicklungsgeschwindigkeit bei hoher Qualität garantiert werden. Dadurch kann sich das Produktentwicklungsteam mit den wichtigen Dingen beschäftigen, um schneller fachlichen Mehrwert zu erzeugen.

Da Systeme in der Regel durch Zusammenarbeit Aufgaben erfüllen, muss so oft wie möglich getestet werden, ob bisher sichergestellte Funktionalität weiterhin bestand hat. Je früher ein Fehler erkannt wird, desto günstiger ist seine Behebung. Tests an der Software sind zwar anfangs teuer, die Kosten von späten Anpassungen durch erkannte Fehler übersteigen diesen Preis aber exponentiell. Da das Ausführen aller Unit- und Integrationstests in einem Projekt schnell sehr viel Zeit in Anspruch nimmt, müssen diese automatisierbar ausgeführt werden können. Nur so kann gewährleistet werden, dass die Tests im Entwicklungsprozess häufig ausgeführt werden. Erst mit der Nutzung kann man in Systemen Fehler und Fehlverhalten durch Sicherheitslücken oder zu hohe Last betrachten, da der Entwicklungsprozess in einem geschlossenen System passiert. Um diese Fehler früher zu erkennen, setzen wir auf automatisierte Sicherheits- Compliance- und Lasttests. Damit stellen wir sicher, dass die Erkenntnisse nicht erst spät in den Entwicklungsprozess einfließen und zu teuren Änderungen führen.

(pause: 1)

Manuelle Änderungen an der Infrastruktur und deren Konfiguration sind eine große Fehlerquelle und führen zu Kopfmonopolen. Erfahrungsgemäß werden diese Anpassungen auch nicht oder nur unzureichend dokumentiert. Damit weichen die verschiedenen Umgebungen voneinander ab und das Verhalten der Software scheint auf unerklärliche Weise nicht mehr reproduzierbar. Um dies zu vermeiden, setzen wir auf eine hohe Automatisierung der Infrastruktur.

(pause: 1)

Teams die Änderungen seltener und in größeren Posten auf Produktion deployen, verursachen mehrere Probleme. Die geringe Agilität reduziert Feedback-Zyklen und Marktfähigkeit. Ein unterbewertetes Problem ist menschlicher Natur. Das Team entwickelt einen unerwünschten Respekt vor Deployments, da der Abstand zwischen Deployments, die Fehlerhäufigkeit und die Auswirkung von möglichen Fehlern größer werden. Dies verursacht Stress im Team, der mittlerweile im Fachjargon “Deployment Pain” genannt wird. Deployment Pain führt zu Demotivation, weniger Leistung und sogar Burnouts. Deshalb glauben wir, dass Releases auf Produktion häufig und automatisiert erfolgen sollen.  Dadurch liefern wir häufiger fachlichen Mehrwert aus und reduzieren den Stress im Team. Damit erhöht sich sowohl die Motivation als auch die Performance des Teams.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie14.png)

Punkt fünf: Kontinuierliche Verbesserung

Kontinuierliche Verbesserung auf technischer sowie persönlicher Ebene sehen wir als Kern unserer Arbeit an. Nur wer ständig Neues lernt und sich weiterentwickelt kann Vorsprung für sich und seine Kunden generieren.

(pause: 1)

Aus diesem Grund sind Retrospektiven für uns unverzichtbar. Durch das gemeinsame Reflektieren und Brainstorming ermöglichen wir mit Retrospektiven eine kontinuierliche Verbesserung im Team und schaffen einen zentralen Punkt, um die Konflikte – welche der Verbesserung im Wege stehen können – transparent und offen anzugehen. Durch regelmäßige Retrospektiven werden aber nicht nur Hindernisse oder Verbesserungsvorschläge, sondern auch Erfolge und Ergebnisse, auf die man stolz sein kann, thematisiert und der aktuelle Eindruck der Arbeit im Team validiert.

Unsere Überzeugung ist, dass der Code dem gesamten Team gehört. Durch Code-Reviews lässt sich erreichen, dass die Code-Qualität durch das 4-Augen-Prinzip steigt, da die Konzeptionen eines jeden Entwicklers durch einen unbeteiligten Entwickler nochmals herausgefordert werden. Durch das Teilen des Codes in der Code-Review erfolgt darüberhinaus ein Wissenstransfer des Entwicklers hin zu mindestens einem weiteren Entwickler. Dadurch lassen sich Kopfmonopole effektiv vermeiden und die Reaktionsgeschwindigkeit bei Problemen, so wie die Kenntnis der Codebasis im Team verbessern. Letztendlich tragen Code-Reviews auch dazu bei, den Code zu vereinheitlichen, so dass die gesamte Code-Basis homogener wird.

(pause: 1)

Neben einer Review des Codes muss auch die Software-Architektur regelmäßig überprüft werden. Dafür ist neben den Architekten auch das Entwicklerteam notwendig. Denn gerade die Entwickler tragen mit ihrer täglichen Arbeit zur Architektur im Kleinen bei. Ein zentraler Termin in regelmäßigen Abschnitten hilft zum einen Transparenz über die wichtigen Kriterien der Architektur und den damit begründeten Entscheidungen zu schaffen. Zum Anderen wird damit auch ein offener Austausch zwischen Entwicklerteam und Architekten garantiert. Auch hier lassen sich somit wieder Kopfmonopole auflösen, Fehlkonzeptionen aufdecken und die allgemeine Produktqualität erhöhen.

(pause: 1)

Da der Code und die Software-Architektur nur Mittel für die Erfüllung von Kundenanforderungen sind, bedarf es auch für diese Anforderungen und Konzepte einer regelmäßige Review. Hierfür eigenen sich User Research Analysen besonders. Mithilfe von User Research Analysen können wir bei bereits ausgerollten Inkrementen regelmäßig die Anforderungen Wünsche und Herausforderungen der Nutzer mit dem System erheben. Damit lassen sich Einblicke generieren, wie die weitere Produkt-Strategie auszurichten ist. Auch Kommunikationsstrategien und Marketing-Kampagnen lassen sich auf Basis der User Research Analyse besser planen und re-validieren.

(pause: 3)
---
![contain](slides/phase-initiate-develop/Folie15.png)

Wir knüpfen nach den Agilen Werten nun an mit dem Thema agile Langzeitplanung:

(pause: 1)

Agile Langzeitplanung garantiert die Ausrichtung der Produktentwicklung auf ein gemeinsames Zielbild. Dieses wird um einen konkreten Releaseplan ergänzt, welcher die zu erwartenden Lieferungen einzelner Features auf eine prognostizierte Zeitachse bringt und mit Lieferterminen versieht. Zielbild und Releaseplan werden zunächst initial definiert und dann periodisch, auf Basis aktueller Erkenntnisse im Projekt aktualisiert. Vom Releaseplan leiten sich die Ziele eines jeden Releases ab und er definiert die Backlog Items mit der höchsten Priorität. Je weiter die Releaseplanung in die Zukunft erfolgt, desto weniger Details des Zielbilds sind enthalten und desto größer sind die Unsicherheiten. Damit aus Zielbild und Releaseplan eine steuerbare Langzeitplanung werden, müssen diese kontinuierlich, auf Basis der neuesten Projekt-Erkenntnisse aktualisiert werden.

(pause: 2)

---
![contain](slides/phase-initiate-develop/Folie16.png)

Der Bereich Produktentwicklung sieht folgendes vor:

Die Basis für eine iterative Produktentwicklung und die konstante Generierung von Vorsprung ist der P D S A (Plan, Do, Study, Act) Zyklus. Hierbei geht es darum die nächsten Schritte anhand vorhandener Informationen und Hypothesen zu planen und durchzuführen. Im Anschluss werden die Ergebnisse betrachtet und Verbesserungen abgeleitet.
Gemeinsames Verständnis & Agiles Requirement Engineering bilden den Rahmen für die Produktentwicklung. Im agilen Requirements Engineering fokussieren wir uns immer auf die als nächstes relevanten Kundenanforderungen. Die Anforderungen werden zunächst als Hypothesen betrachtet, die es nach der Umsetzung zu validieren gilt.

(pause: 1)

Scrum ist ein leichtgewichtiges Produktentwicklungsframework für Teams und Organisationen. Das Ziel ist es konstanten Wert für den Kunden in kurzen Entwicklungszyklen (sogenannten Sprints oder Iterationen) zu liefern. Als Rahmenwerk überlässt Scrum dem Produktteam, unter Anleitung des Scrum Masters, die detaillierte Ausgestaltung des eigenen Prozesses.
Retrospektiven & Validierung sind zentrale Werkzeuge der agilen Vorgehensmodelle. Nach jeder Iteration werden Potentiale im Team, Prozess oder dem Produkt erarbeitet und entsprechende Maßnahmen definiert. Mit Unterstützung des Scrum Masters werden die Maßnahmen vom Produktteam umgesetzt, welches sich so zu einem High-Performance-Team weiterentwickelt.

(pause: 2)
---
![contain](slides/phase-initiate-develop/Folie17.png)

Zum Schluss noch einmal schnell zusammengefasst:

(pause: 1)

Die Initiate und Develop Faahse hat einiges zu bieten und ist das Herzstück des Frameworks. Erinnert euch am besten an die vier Kernbereiche der Phase:

Bereich eins: Die Akteure und Ihre Rollen im Framework.
Bereich zwei: Agile Essentials.
Bereich drei: die Agile Langzeitplanung, und schließlich
Bereich vier: Die iterative Produktentwicklung.

(pause: 1)

Vielen Dank fürs Zuhören, auch wenn es heute etwas länger ging, bleibt dran!
Ilse bedankt sich bei den Zuschauern und Lernenden, wir hören uns zur [Transition]{en} Faahse. Bis Bald!

(pause: 1)
---
