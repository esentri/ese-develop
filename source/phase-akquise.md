---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/phase-akquise/Folie1.png)

Hallo und herzlich willkommen zur Video Serie über das Framework easy deewellopp.

(pause: 3)

Im Intro habt ihr bereits eine Übersicht über die verschiedenen Phasen erhalten. Im Folgenden betrachten wir die Akquise Phase im Detail. Die Akquise ist dem eigentlichen easy deewellopp Prozess vorangestellt. Sie beinhaltet alle Vorhaben um ein Projekt für uns zu gewinnen. Vom Erstkontakt mit dem Kunden bis zur Vertragsunterzeichnung sprechen wir von der Akquise.

(pause: 1)

Auch wenn das Akquirieren nicht Dein Tagesgeschäft ist, ist es interessant zu verstehen wie der Prozess in groben Zügen abläuft und welche langfristigen Auswirkungen er auf ein Projekt hat.

(pause: 2)
---
![contain](slides/phase-akquise/Folie2.png)


In der Akquise Phase stehen verschiedene Artefakte zur Verfügung. Zunächst das Sales Deck. Hierbei handelt es sich um Folien, die die wichtigsten Punkte des easy deewellopp Frameworks beschreiben. Das Sales Deck wird genutzt um Kunden eine Vorstellung zu geben, wie ein Projekt mit issentri abläuft.

Das zweite Artefakt ist der sogenannte Request for Proposal Prozess.


(pause: 1)
---
![contain](slides/phase-akquise/Folie3.png)

Der RFP Prozess ermöglicht in einfachen Schritten eine Angebotsanfrage zu erstellen. Der Prozess beginnt mit einem Kickoff. In diesem werden die wichtigsten Projektfragen geklärt. Im zweiten Schritt geht es um die Vorbereitung. Alle Teilnehmenden bekommen dieselben Unterlagen, um ein gemeinsames Projekt-Bild zu schaffen. Anschließend beginnt die gemeinsame Vorbereitung in Clustern. Diese Cluster sind

(pause: 1)

Erstens, Fachliche Themen. Wie lauten die Anforderungen? Welche Module gibt es? Und wie können diese in Epics unterteilt werden.

Zweitens, Risiken und Risiko Cluster. Aspekte können sein: allgemeine Risiken der IT, konkrete Risiken auf die Angebotsanfrage bezogen, Risikocluster und Highlight-Anforderungen die diese Risiken begründen.  

Drittens, Architektur-Fragen. Beispielsweise wie bedient die hexa gonale Architektur die Anforderungen? Gibt es bereits kundenspezifische Dienstleistungen? Oder welche Anforderungen treiben die Design-Entscheidungen.

Viertens, Skills und Teams. Welches Knowhow wird für das Projekt benötigt? Aus wie viel Personen soll das Projekt bestehen und wie viele werden als Grundlage für die Schätzung angenommen.

Fünftens und zuletzt, offene Fragestellungen. Gibt es welche? Welche Lücken können identifiziert werden? Was verhindert eine konkrete Schätzung? Welche Prämissen müssen für die Schätzung getroffen werden? Oder was muss im Laufe des Projektes geklärt werden?

(pause: 1)

 Alle Teilnehmenden beschäftigen sich im Vorfeld mit diesen Clustern und Fragestellungen. In einem gemeinsamen, circa zweistündigen Termin werden diese diskutiert, und dokumentiert.

 (pause: 1)

 Abschließend für den RFP Prozess geht es um die Release Planung. In einem dritten und letzten Termin werden die Release Planung sowie die Schätzung vorbereitet. Folgende Fragen könnten geklärt werden:

  (pause: 1)

 Welche Epics sollen während der Initiate Phase umgesetzt werden?
 Welchen Hypothesen dient diese Umsetzung?
 Was bekommt der Kunde am Ende konkret?
 Und schließlich wie lange das Ganze in Personentagen dauert.  

 (pause: 1)

 Der gesamte RFP Prozess soll für den einzelnen Teilnehmer maximal acht Stunden an Aufwand bedeuten.

  (pause: 1)

 Wichtig für Phase 2 und 3 des RFP Prozesses ist die individuelle Vorbereitung vorab, um in gemeinsamen Terminen schnell zu einem Ergebnis zu kommen.

 (pause: 2)
 ---
 ![contain](slides/phase-akquise/Folie2.png)

 Das dritte und letzte Artefakt der Akquise Phase ist ein Sheet um die Release Planung und Schätzung zu unterstützen. Die Projektdauer soll in einer Bandbreitenschätzung ermittelt werden.

 (pause: 1)
 ---
 ![contain](slides/phase-akquise/Folie4.png)

 Die bereits erarbeiteten Anforderungen in Epics oder Modulen geben die Granularität der Schätzung vor. Pro Modul kann ein Minimal und Maximalaufwand in Personentagen geschätzt werden. Daraus wird der Mittelwert berechnet.

  (pause: 1)

 Zusätzlich können die im RFP Prozess definierten Risiken mit in die Schätzung einfließen. Diese können Schätzungen nach oben korrigieren, den Maximalaufwand begründen oder zur Planung der Umsetzung herangezogen werden. Beispielsweise können Risiken frühzeitig angegangen werden, um Aufwände im späteren Verlauf des Projektes zu reduzieren.

 (pause: 1)
 ---
 ![contain](slides/phase-akquise/Folie5.png)

 Auf Basis von Personentagen-Schätzungen pro Modul, werden die easy deewellopp Projektphasen berechnet. Sprint- und Release-Umfang können frei konfiguriert werden. Feiertage werden berücksichtigt.

 (pause: 3)
 ---
 ![contain](slides/phase-akquise/Folie2.png)

 Das waren die wichtigsten Bestandteile der Akquise Phase.

  (pause: 1)

 Akquise ist wichtig. Alle bei issentri sollen ein Bewusstsein für die Bedeutung sowie grobe Kenntnis über die Inhalte haben.

 (pause: 1)

 Vielen Dank fürs Zuhören. Ich wünsche viel Freude beim weiteren Kennenlernen des easy deewellopp Frameworks.

---
