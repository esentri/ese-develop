---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.02 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/phase-transition/Folie1.png)

(pause: 3)

Hallo und herzlich willkommen zurück zur easy deewellopp Lernreihe.
Wie schon in den vorherigen Videos führe ich euch heute durch die Inhalte. Ich freue mich, dass ihr wieder dabei seid.
Heute lernen wir die [Transition]{en} kennen, sie bildet die abschließende Phase des easy deewellopp Frameworks.
Und schon geht es los!
---
(pause: 2)

![contain](slides/phase-transition/Folie2.png)

Wenn eine Person, eine Gruppe von Menschen oder auch eine Organisation einen Wandel vornimmt, entsteht ein unruhiger Zustand. Verwirrung, Stress und Unsicherheit sind oftmals Begleiterscheinungen dieses Zustandes. Mit der [Transition]{en} versuchen wir diese natürlichen Prozesse kontrolliert und zielgerichtet ablaufen zu lassen.
---
(pause: 2)

![contain](slides/phase-transition/Folie3.png)

Gegen Ende eines Projektes gilt es das entwickelte Produkt an den Kunden zu übergeben. Eine hohe Qualität des Produktes und seiner Dokumentation sind dabei ein guter Anfang, reichen aber nicht aus, damit der Kunde sein Produkt am Ende des Projektes betreiben und selbst weiterentwickeln kann. Kundenmitarbeiter müssen befähigt und die nötigen Prozesse etabliert werden. Um diesen Prozess zu gestalten existiert innerhalb von easy deewellopp die [Transition]{en}. Während der [Transition]{en} nutzen wir Theorien und Prinzipien aus dem Change- und Projekt-Management, um eine effektive und nachhaltige Übergabe an den Kunden zu gewährleisten. Es stehen drei Aspekte im Fokus.

(pause: 1)

Erstens: Abschluss der Produktentwicklung.
Zweitens: Enablement des Kunden.
Und drittens Überführung in die Linie des Kunden.

Die [Transition]{en} ist dabei nach dem Prinzip eines Übergangsrituals gestaltet und besteht selbst wiederum aus drei Phasen und zwei Events die wir uns nun etwas genauer anschauen wollen.
---
(pause: 2)

![contain](slides/phase-transition/Folie4.png)

Erste Phase: Preparation. Innerhalb der Preparation  Faahse  wird der Übergang mit dem Kunden geplant. Die richtigen Kundenmitarbeiter werden identifiziert und gemeinsam mit dem Leadership die Kommunikation vorbereitet. Die identifizierten Mitarbeiter werden anschließend zur Teilnahme eingeladen und in den entsprechenden Bereichen von uns geschult.

(pause: 1)

Zweite Phase:  [Transition]{en}. Die Phase in der konkret an dem Übergang gearbeitet wird. Die Kundenmitarbeiter werden an der aktiven Weiterentwicklung des Produktes beteiligt und lernen so im Alltag. Wir dienen in dieser Phase als Coaches und Trainer. Zum Ende dieser Phase hin sollte die operative Arbeit primär durch die Kundenmitarbeiter geleistet werden. Parallel zum Enablement der Kundenmitarbeiter werden noch die im Kick-Off identifizierten Herausforderungen gelöst.

(pause: 1)

Dritte Phase: Inspect & Adapt. Die Übergabe an den Kunden ist abgeschlossen. Der Kunde selbst beschäftigt sich mit der Umsetzung der in der Retrospektive definierten Maßnahmen. Der Betrieb und die Weiterentwicklung des Produktes wird vom Kunden selbst vorangetrieben. Wir führen regelmäßige Health Checks durch, um eventuell auftretende Probleme zu erkennen und gegebenenfalls gegenzusteuern.
---
(pause: 2)

![contain](slides/phase-transition/Folie5.png)

Während den Phasen sind zwei Events geplant:

Erstens, das Kick-Off Event.

(pause: 1)

Mit dem Kick-Off startet die geplante [Transition]{en} mit allen beteiligten Akteuren. Das Leadership kommuniziert die vorher geplanten Informationen. Innerhalb des Kick-Offs sollen außerdem offene Fragen geklärt werden. Zuletzt wird eine Liste von aktuellen Herausforderungen mit allen Teilnehmern erarbeitet, die eine nachhaltige Übergabe des Produktes und dazugehöriger Prozesse verhindern könnten.

(pause: 1)

Zweitens, das Rite of Passage Event: Nach der [Transition]{en} Phase wird eine größere Retrospektive als Rite of Passage durchgeführt. Innerhalb der Retrospektive betrachtet man die [Transition]{en}, zeigt auf welche Herausforderungen gelöst werden konnten und übergibt den "Staffelstab" an den Kunden. In der letzten Hälfte des Workshops werden aktuelle Herausforderungen vom Kunden identifiziert und entsprechende Maßnahmen definiert. Die Umsetzung der Maßnahmen ist vom Kunden selbst zu leisten.

---
(pause: 2)

![contain](slides/phase-transition/Folie6.png)

Zum Schluss noch einmal die wichtigsten Punkte zusammengefasst: Die [Transition]{en} bildet den Abschluss des Frameworks.

(pause: 1)

Erstens: Ein kontrollierter und zielgerichteter Übergabeprozess befähigt den Kunden auch ohne unsere Unterstützung das entstandene Produkt zu betreiben.
Zweitens: Der Übergabeprozess ist in die drei Phasen: Preparation, [Transition]{en}, Inspect & Adapt aufgeteilt.
Drittens: Mit dem Ende der [Transition]{en} übernimmt der Kunde die Verantwortung über das Produkt.

(pause: 2)

Und das war es heute auch schon wieder.
Vielen lieben Dank fürs Zuhören!
Ich bedanke mich bei allen Lernenden und verabschiede mich hiermit recht herzlich, eure Ilse!
---
